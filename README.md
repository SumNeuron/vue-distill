# Vue-Distill
This is a template repository for a Vue based Distill publication.
The directory `dev` is used for prototyping your vue components.
The publication itself should be written in `rollup/index.html`.
When your components are finalized, then roll them up by modifying
`dev/src/rollup.entry.js` and calling `npm run build:r`.



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Serve production build
```
npm run serve:p
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
