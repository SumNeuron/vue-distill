import Vue from 'vue'
// import App from './App.vue'
import VuetifyBtn from './components/VuetifyBtn.vue'
import VuetifyBtnVApp from './components/VuetifyBtnVApp.vue'
import VuetifyBtnALaCarte from './components/VuetifyBtnALaCarte.vue'
import VuetifyBtnALaCarteVApp from './components/VuetifyBtnALaCarteVApp.vue'
import WrapBtn from './components/WrapBtn.vue'
import store from './store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

new Vue({
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

// new Vue({
//   store,
//   vuetify,
//   render: h => h(VuetifyBtn)
// }).$mount('#btn1')
// new Vue({
//   store,
//   vuetify,
//   render: h => h(VuetifyBtnVApp)
// }).$mount('#btn2')
// new Vue({
//   store,
//   vuetify,
//   render: h => h(VuetifyBtnALaCarte)
// }).$mount('#btn3')
// new Vue({
//   store,
//   vuetify,
//   render: h => h(VuetifyBtnALaCarteVApp)
// }).$mount('#btn4')
// new Vue({
//   store,
//   vuetify,
//   render: h => h(WrapBtn)
// }).$mount('#btn5')
