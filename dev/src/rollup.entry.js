import Vue from 'vue';
import Vuetify from 'vuetify/lib'

import wrap from '@vue/web-component-wrapper'
import slugify from 'slugify'

Vue.use(Vuetify)

import VuetifyBtn from './components/VuetifyBtn.vue'
import VuetifyBtnVApp from './components/VuetifyBtnVApp.vue'
import VuetifyBtnALaCarte from './components/VuetifyBtnALaCarte.vue'
import VuetifyBtnALaCarteVApp from './components/VuetifyBtnALaCarteVApp.vue'
import WrapBtn from './components/WrapBtn.vue'


// setup for to loop over and register
const components = {
  VuetifyBtn,
  VuetifyBtnVApp,
  VuetifyBtnALaCarte,
  VuetifyBtnALaCarteVApp,
  WrapBtn,
}


// BEGIN: @vue/web-component-wrapper
// helpers for registering with window
const pascalToKebabHelper = (x, y) => `-${y.toLowerCase()}`
const pascalToKebab = (str) => {
  return str.replace(/\.?([A-Z]+)/g, pascalToKebabHelper).replace(/^-/, "")
}
// define webcomponents
if (typeof window !== 'undefined') {
  Object.keys(components).forEach(name=>{
    let htmlTagName = pascalToKebab(name)
    let vueWebComponent = wrap(Vue, components[name])
    window.customElements.define(htmlTagName, vueWebComponent)
  })
}
// END: @vue/web-component-wrapper


// global register components with Vue
function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });
}

const plugin = { install, }

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
  GlobalVue.use(Vuetify)
}


export const strict = false
// export all components by default
export default components
// ecport each component individually
export {
  VuetifyBtn,
  VuetifyBtnVApp,
  VuetifyBtnALaCarte,
  VuetifyBtnALaCarteVApp,
  WrapBtn,
}
