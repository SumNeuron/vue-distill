// rollup.config.js
import vue from 'rollup-plugin-vue';
import babel from 'rollup-plugin-babel';
import { terser } from "rollup-plugin-terser";
import minimist from 'minimist';
import async from 'rollup-plugin-async';

import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import postcss from 'rollup-plugin-postcss'

import simplevars from 'postcss-simple-vars';
import nested from 'postcss-nested';
import cssnano from 'cssnano';
import css from 'rollup-plugin-css-only'


import injectProcessEnv from 'rollup-plugin-inject-process-env';


import pkg from './package.json';



const argv = minimist(process.argv.slice(2));


const config = {
  input: './dev/src/rollup.entry.js',

  external: [
    'vue', 'Vue'
  ],
  output: {
    name: 'pub',
    exports: 'named',
    globals: {
      'vue': 'Vue',
      // 'vuetify': 'Vuetify',
      'Vuetify': 'Vuetify',
    },
  },
  plugins: [
    css(), // convert <style> to inpmport statements

    vue({
      css: false,
      compileTemplate: true,
      template: { optimizeSSR: false },
    }),

    async(),

    postcss({
      extensions: ['.css'],
      plugins: [
        simplevars(),
        nested(),
        cssnano(),
      ]
    }),

    nodeResolve({
      mainFields: [
        'module', 'main', 'jsnext',
      ]
    }),

    commonjs({
      // non-CommonJS modules will be ignored, but you can also
      // specifically include/exclude files
      include: 'node_modules/**',  // Default: undefined

      // search for files other than .js files (must already
      // be transpiled by a previous plugin!)
      extensions: [ '.js', '.coffee' ],  // Default: [ '.js' ]


      namedExports: {
      },  // Default: undefined

      // sometimes you have to leave require statements
      // unconverted. Pass an array containing the IDs
      // or a `id => boolean` function. Only use this
      // option if you know what you're doing!
      ignore: [ 'conditional-runtime-dependency' ]
    }),

    babel({
      exclude: 'node_modules/**',
      externalHelpers: true,
      runtimeHelpers: true,
      plugins: [
        ['wildcard', { exts: ['vue'], nostrip: true, },],
        // '@babel/plugin-external-helpers',
        // '@babel/plugin-transform-runtime'

      ],
      presets: [
        ['@babel/preset-env', { modules: false, },],
      ],
    }),

   injectProcessEnv({
       NODE_ENV: process.env.NODE_ENV,
    }),
  ],
};


// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(terser());
}

export default config;
